package com.handmark.pulltorefresh.library;

import android.view.MotionEvent;
import android.view.View;

public interface OnViewTouchEventListerner {
	public void onListDispatchTouchEvent(View v, MotionEvent ev);
}
