package com.handmark.pulltorefresh.library;


public interface PullToRefreshStateListener {
	public abstract void onPullToRefresh();
	public abstract void onRefreshing();
	public abstract void onReleaseToRefresh();
	public abstract void onReset();
	
}
