package com.handmark.pulltorefresh.library;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;


public class DispatchTouchEventListView extends ListView{
	public DispatchTouchEventListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	private OnViewTouchEventListerner onListViewTouchEventListerner;
	
	public void setOnListViewTouchEventListerner(OnViewTouchEventListerner onListViewTouchEventListerner){
		this.onListViewTouchEventListerner = onListViewTouchEventListerner;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(onListViewTouchEventListerner != null)
			onListViewTouchEventListerner.onListDispatchTouchEvent(this, ev);
		return dispatchTouchEventRealOperation(ev);
	}
	
	public boolean dispatchTouchEventForExternalCall(MotionEvent ev) {
		return dispatchTouchEventRealOperation(ev);
	}
	
	private boolean dispatchTouchEventRealOperation(MotionEvent ev) {
		/**
		 * This is a bit hacky, but Samsung's ListView has got a bug in it
		 * when using Header/Footer Views and the list is empty. This masks
		 * the issue so that it doesn't cause an FC. See Issue #66.
		 */
		try {
			boolean result = super.dispatchTouchEvent(ev); 
			return result;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return false;
		}
	}
}
