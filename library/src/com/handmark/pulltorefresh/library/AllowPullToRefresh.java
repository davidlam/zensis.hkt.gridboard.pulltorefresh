package com.handmark.pulltorefresh.library;

public interface AllowPullToRefresh{
	abstract boolean allowPullToRefresh();
}
