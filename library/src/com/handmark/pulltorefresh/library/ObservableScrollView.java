package com.handmark.pulltorefresh.library;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class ObservableScrollView extends ScrollView {

    private ScrollViewListener scrollViewListener = null;
    private boolean enableScrolling = true;
    public ObservableScrollView(Context context) {
        super(context);
    }

    public ObservableScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ObservableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScrollViewListener(ScrollViewListener scrollViewListener) {
        this.scrollViewListener = scrollViewListener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if(scrollViewListener != null) {
            scrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
        }
    }
    @Override
    public void fling(int velocityY) {
    	super.fling((int) (velocityY/1.5));
    }
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if (enableScrolling) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        if (enableScrolling) {
            return super.onTouchEvent(ev);
        } else {
            return false;
        }
    }
    
    public void setEnableScrolling(boolean enableScrolling) {
        this.enableScrolling = enableScrolling;
    }
}
