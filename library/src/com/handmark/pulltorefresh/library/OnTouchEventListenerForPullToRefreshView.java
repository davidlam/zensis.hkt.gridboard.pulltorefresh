package com.handmark.pulltorefresh.library;

import android.view.MotionEvent;
import android.view.View;

public interface OnTouchEventListenerForPullToRefreshView {
	public abstract boolean onTouchEventForPullToRefreshListView(View view, MotionEvent event);
	public abstract boolean onInterceptTouchEventForPullToRefreshListView(View view, MotionEvent event);
}
