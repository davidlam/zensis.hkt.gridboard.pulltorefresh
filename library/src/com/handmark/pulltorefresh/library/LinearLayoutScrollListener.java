package com.handmark.pulltorefresh.library;

import android.view.View;

public interface LinearLayoutScrollListener {
    void onScrollChanged(PullToRefreshBase view, int x, int y, int oldx, int oldy);
}
